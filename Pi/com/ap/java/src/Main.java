import solution_1.main_of_solution_1;
import solution_2.main_of_solution_2;

import java.util.ArrayList;
import java.util.Scanner;
import java.math.BigDecimal;
import java.lang.Math;
public class Main {
    public static void main(String[] args)throws InterruptedException
    {
        Scanner in = new Scanner(System.in);
        System.out.println("if you want solution1 to computing Pi number press 1 and if you want solution2(monte-carlo) press 2: ");
        int choice = in.nextInt();
        if (choice == 1)
        {
            main_of_solution_1 origin_1 = new main_of_solution_1();
            origin_1.main_of_solution_1();
        }
        else if (choice == 2)
        {
            main_of_solution_2 origin_2 = new main_of_solution_2();
            origin_2.main_of_solution_2();
        }
    }
}
