package solution_2;


import java.util.ArrayList;
import java.util.Scanner;
import java.lang.Math;

public class main_of_solution_2 {
    public void main_of_solution_2()throws InterruptedException
    {
        Scanner in = new Scanner(System.in);
        System.out.println("enter number of the points that program can put: ");
        int n = in.nextInt();
        System.out.println("enter the radius of monte-carlo circle: ");
        int r = in.nextInt();
        Long time_2 =(Long)(long) 0;
        Long time_1 = System.nanoTime();
        time_2=solution_2(n,r);
        System.out.println("time: "+(time_2-time_1)/1000000.0+"ms");
        Long time_4 = System.nanoTime();
        Long time_3 = System.nanoTime();
        time_4 = solution_2_with_Thread(n,r);
        System.out.println("time: "+(time_4-time_3)/1000000.0+"ms");
    }
    public Long solution_2(int n,int r)
    {
        int circle_area = 0;
        int square_area = n;
        for (int i=0;i<n;i++)
        {
            double x_point = Math.random();
            double y_point = Math.random();
            double x = x_point*(2*r);
            double y = y_point*(2*r);
            if (Math.sqrt(Math.pow((x-r),2)+Math.pow((y-r),2)) <= r)
            {
                circle_area++;
            }
        }
        double Pi = ((double)circle_area/(double)square_area)*4;
        Long time_2 = System.nanoTime();
        System.out.println(Pi);
        return time_2;
    }
    public Long solution_2_with_Thread(int n,int r) throws InterruptedException
    {
        share_object answer = new share_object();
        int i = 0;
        ArrayList<Thread> basket = new ArrayList<>();
        while (i+10<=n)
        {
            Runnable Threads = new solution_2_with_Thread(i,i+10,answer,r);
            Thread one = new Thread(Threads);
            basket.add(one);
            one.start();
            i = i+10;
        }
        Runnable Threads = new solution_2_with_Thread(i,n+1,answer,r);
        Thread one = new Thread(Threads);
        basket.add(one);
        one.start();
        for (Thread t : basket)
            t.join();
        Long time_4 = System.nanoTime();
        System.out.println("with Threading: "+((double)answer.getcircle_area()/(double)n)*4);
        return time_4;
    }
}
