package solution_2;

import java.math.BigDecimal;

public class solution_2_with_Thread implements Runnable {
    private int begin;
    private int end;
    private share_object pi;
    private int r;
    public solution_2_with_Thread(int first, int second, share_object temporary,int r)
    {
        begin = first;
        end = second;
        pi = temporary;
        this.r = r;
    }
    public void run()
    {
        for (int i=0;i<end-begin;i++)
        {
            double x_point = Math.random();
            double y_point = Math.random();
            double x = x_point*(2*r);
            double y = y_point*(2*r);
            if (Math.sqrt(Math.pow((x-r),2)+Math.pow((y-r),2)) <= r)
            {
                pi.add();
            }
        }
    }
}
