package solution_2;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class share_object {
    private int circle_area;
    private Lock lock;
    public share_object()
    {
        lock = new ReentrantLock();
        circle_area=0;
    }
    public void add()
    {
        lock.lock();
        circle_area++;
        lock.unlock();
    }
    public int getcircle_area()
    {
        return circle_area;
    }
}
