package solution_1;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Scanner;

public class main_of_solution_1 {
    public void main_of_solution_1()throws InterruptedException
    {
        Scanner in = new Scanner(System.in);
        System.out.println("enter number of the calculations: ");
        int n = in.nextInt();
        Long time_2 =(Long)(long) 0;
        Long time_1 = System.nanoTime();
        time_2=solution_1(n);
        System.out.println("time: "+(time_2-time_1)/1000000.0+"ms");
        Long time_4 = System.nanoTime();
        Long time_3 = System.nanoTime();
        time_4 = solution_1_with_Thread(n);
        System.out.println("time: "+(time_4-time_3)/1000000.0+"ms");
    }
    public Long solution_1(int n)
    {
        BigDecimal Pi = new BigDecimal("0.0");
        for (double i =0.0;i<=n;i = i+1)
        {
            BigDecimal temporary = BigDecimal.valueOf(((4/(8*i+1))-(2/(8*i+4))-(1/(8*i+5))-(1/(8*i+6)))*(1/Math.pow(16,i)));
            Pi = Pi.add(temporary);
        }
        Long time_2 = System.nanoTime();
        System.out.println(Pi);
        return time_2;
    }
    public Long solution_1_with_Thread(int n) throws InterruptedException
    {
        share_object answer = new share_object();
        int i = 0;
        ArrayList<Thread> basket = new ArrayList<>();
        while (i+10<=n)
        {
            Runnable Threads = new solution_1_with_Thread(i,i+10,answer);
            Thread one = new Thread(Threads);
            basket.add(one);
            one.start();
            i = i+10;
        }
        Runnable Threads = new solution_1_with_Thread(i,n+1,answer);
        Thread one = new Thread(Threads);
        basket.add(one);
        one.start();
        for (Thread t : basket)
            t.join();
        Long time_4 = System.nanoTime();
        System.out.println("with Threading: "+answer.getPi());
        return time_4;
    }
}
