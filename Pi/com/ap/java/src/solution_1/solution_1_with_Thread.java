package solution_1;

import java.math.BigDecimal;

public class solution_1_with_Thread implements Runnable {
    private int begin;
    private int end;
    private share_object pi;
    public solution_1_with_Thread(int first,int second,share_object temporary)
    {
        begin = first;
        end = second;
        pi = temporary;
    }
    public void run()
    {
        BigDecimal piece = new BigDecimal("0.0");
        for (double j = begin;j<end;j++)
        {
            piece = piece.add(BigDecimal.valueOf(((4/(8*j+1))-(2/(8*j+4))-(1/(8*j+5))-(1/(8*j+6)))*(1/Math.pow(16,j))));
        }
        pi.add(piece);
    }
}
