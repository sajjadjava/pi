package solution_1;
import java.math.BigDecimal;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class share_object {
    private  BigDecimal Pi;
    private Lock my_lock;
    public share_object()
    {
        Pi = new BigDecimal("0.0");
        my_lock = new ReentrantLock();
    }
    public void add(BigDecimal number)
    {
        my_lock.lock();
        Pi = Pi.add(number);
        my_lock.unlock();
    }
    public BigDecimal getPi()
    {
        return Pi;
    }
}